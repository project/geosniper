<?php

/**
 * @file
 * Information to populate the GeoSniper block.
 */
function geosniper_get_geo($geosniper_key, $geosniper_lang) {
// IP address of the user viewing the page
$geosniper_ip = $_SERVER["REMOTE_ADDR"];
// Personal Query Key if you wish to increase your daily allotted lookup quota (like 5 EUR for 10,000 lookups or something which isn't too bad...)
// To get one, go to infosniper.net
$geosniper_key = variable_get('geosniper_key', '');
// URL to GET from
$geosniper_url = "http://www.infosniper.net/xml.php?ip_address=". $geosniper_ip;

// If no key is given, exclude it from the above GET request
  if($geosniper_key != "") {
    $geosniper_url = $geosniper_url ."&k=". $geosniper_key;
  }

// Language stuff
  if($geosniper_lang == "espanol") {
    $geosniper_lang = "&lang=3";
  }
  elseif ($geosniper_lang == "deutsch") {
  $geosniper_lang = "&lang=2";
  }
  else {
  $geosniper_lang = "&lang=1";
  }

// Get geolocation info from infosniper.net...
  $geosniper_url = $geosniper_url . $geosniper_lang;
  $geosniper_str = file_get_contents($geosniper_url);
// ... and say "I Heart SimpleXML"  ;)
  $geoxml = new SimpleXMLElement($geosniper_str);

// A little markup for flag images
  $flagopen = "<img src='";
  $flagclose = "'/>";

// Lots of stuff going on here... Mostly for compliance with Drupal coding standards
  $geosniper_results = array();
    $geosniper_results["ipaddress"] = t($geoxml->result[0]->ipaddress);
    $geosniper_results["hostname"] = t($geoxml->result[0]->hostname);
    $geosniper_results["provider"] = t($geoxml->result[0]->provider);
    $geosniper_results["country"] = t(htmlentities($geoxml->result[0]->country));
    $geosniper_results["countrycode"] = t(strtoupper($geoxml->result[0]->countrycode));
    $geosniper_results["countryflag"] = $flagopen . $geoxml->result[0]->countryflag . $flagclose;
    $geosniper_results["state"] = t(htmlentities($geoxml->result[0]->state));
    $geosniper_results["city"] = t($geoxml->result[0]->city);
    $geosniper_results["areacode"] = t($geoxml->result[0]->areacode);
    $geosniper_results["postalcode"] = t($geoxml->result[0]->postalcode);
    $geosniper_results["dmacode"] = t($geoxml->result[0]->dmacode);
    $geosniper_results["latitude"] = t($geoxml->result[0]->latitude);
    $geosniper_results["longitude"] = t($geoxml->result[0]->longitude);
    $geosniper_results["queries"] = t($geoxml->result[0]->queries);

  return $geosniper_results;
}
